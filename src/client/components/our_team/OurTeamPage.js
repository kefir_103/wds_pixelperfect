import React from 'react';

import BlankImage from '../../../../public/images/our_team_images/OurTeam_Image_holder.png';
import FacebookLogo from '../../../../public/images/our_team_images/Facebook_Black.png';
import GooglePlusLogo from '../../../../public/images/our_team_images/Google+_Black.png';
import PinterestLogo from '../../../../public/images/our_team_images/Pinterest_Black.png';
import TwitterLogo from '../../../../public/images/our_team_images/Twitter_Black.png';

export const OurTeamPage = () => {
    return (
        <div className={'our-team'}>
            <div className={'our-team__container'}>
                <div className={'our-team__info'}>
                    <div className={'our-team__text-heading'}>Meet the Team</div>
                    <hr />
                    <div className={'our-team__text-desc'}>
                        Sed facilisis non ipsum et interdum. Suspendisse pretium magna sed auctor
                        dictum. Quisque non dignissim metus, non eleifend mi. Proin congue
                        vestibulum elementum. Cras lectus turpis, auctor sit amet elementum at,
                        elementum.
                    </div>
                </div>
                <div className={'our-team__grid'}>
                    <div className={'our-team__element'}>
                        <div className={'our-team__photo'}>
                            <img src={BlankImage} alt={'Member photo'} />
                        </div>
                        <div className={'our-team__name'}>Clay Norman</div>
                        <div className={'our-team__member-desc'}>
                            Donec ultricies sit amet tortor vel commodo. Mauris nisi magna, congue
                            quis faucibus ac.
                        </div>
                        <div className={'our-team__member-contacts'}>
                            <a href={'#'}><img src={TwitterLogo} alt={'Twitter'} /></a>
                            <a href={'#'}><img src={FacebookLogo} alt={'Facebook'} /></a>
                            <a href={'#'}><img src={GooglePlusLogo} alt={'GooglePlus'} /></a>
                            <a href={'#'}><img src={PinterestLogo} alt={'Pinterest'} /></a>
                        </div>
                    </div>
                    <div className={'our-team__element'}>
                        <div className={'our-team__photo'}>
                            <img src={BlankImage} alt={'Member photo'} />
                        </div>
                        <div className={'our-team__name'}>Nathan Walker</div>
                        <div className={'our-team__member-desc'}>
                            Adipiscing auctor turpis. Duis sed lectus placerat, facilisis lacus id,
                            porta lorem. Praesent non consequat at.
                        </div>
                        <div className={'our-team__member-contacts'}>
                            <a href={'#'}><img src={TwitterLogo} alt={'Twitter'} /></a>
                            <a href={'#'}><img src={FacebookLogo} alt={'Facebook'} /></a>
                            <a href={'#'}><img src={GooglePlusLogo} alt={'GooglePlus'} /></a>
                            <a href={'#'}><img src={PinterestLogo} alt={'Pinterest'} /></a>
                        </div>
                    </div>
                    <div className={'our-team__element'}>
                        <div className={'our-team__photo'}>
                            <img src={BlankImage} alt={'Member photo'} />
                        </div>
                        <div className={'our-team__name'}>Valerie Wilson</div>
                        <div className={'our-team__member-desc'}>
                            Fusce nunc risus, fermentum pellentesque dolor at, cursus blandit justo.
                            Nunc sodales nec ante eget.
                        </div>
                        <div className={'our-team__member-contacts'}>
                            <a href={'#'}><img src={TwitterLogo} alt={'Twitter'} /></a>
                            <a href={'#'}><img src={FacebookLogo} alt={'Facebook'} /></a>
                            <a href={'#'}><img src={GooglePlusLogo} alt={'GooglePlus'} /></a>
                            <a href={'#'}><img src={PinterestLogo} alt={'Pinterest'} /></a>
                        </div>
                    </div>
                    <div className={'our-team__element'}>
                        <div className={'our-team__photo'}>
                            <img src={BlankImage} alt={'Member photo'} />
                        </div>
                        <div className={'our-team__name'}>Hope Russell</div>
                        <div className={'our-team__member-desc'}>
                            Nulla in tincidunt neque, a luctus mi. Donec sollicitudin est vehicula
                            mauris condimentum mattis.
                        </div>
                        <div className={'our-team__member-contacts'}>
                            <a href={'#'}><img src={TwitterLogo} alt={'Twitter'} /></a>
                            <a href={'#'}><img src={FacebookLogo} alt={'Facebook'} /></a>
                            <a href={'#'}><img src={GooglePlusLogo} alt={'GooglePlus'} /></a>
                            <a href={'#'}><img src={PinterestLogo} alt={'Pinterest'} /></a>
                        </div>
                    </div>
                    <div className={'our-team__element'}>
                        <div className={'our-team__photo'}>
                            <img src={BlankImage} alt={'Member photo'} />
                        </div>
                        <div className={'our-team__name'}>Pedro Pierce</div>
                        <div className={'our-team__member-desc'}>
                            Donec ultricies sit amet tortor vel commodo. Mauris nisi magna, congue
                            quis faucibus ac.
                        </div>
                        <div className={'our-team__member-contacts'}>
                            <a href={'#'}><img src={TwitterLogo} alt={'Twitter'} /></a>
                            <a href={'#'}><img src={FacebookLogo} alt={'Facebook'} /></a>
                            <a href={'#'}><img src={GooglePlusLogo} alt={'GooglePlus'} /></a>
                            <a href={'#'}><img src={PinterestLogo} alt={'Pinterest'} /></a>
                        </div>
                    </div>
                    <div className={'our-team__element'}>
                        <div className={'our-team__photo'}>
                            <img src={BlankImage} alt={'Member photo'} />
                        </div>
                        <div className={'our-team__name'}>Carolyn Figueroa</div>
                        <div className={'our-team__member-desc'}>
                            Adipiscing auctor turpis. Duis sed lectus placerat, facilisis lacus id,
                            porta lorem. Praesent non consequat at.
                        </div>
                        <div className={'our-team__member-contacts'}>
                            <a href={'#'}><img src={TwitterLogo} alt={'Twitter'} /></a>
                            <a href={'#'}><img src={FacebookLogo} alt={'Facebook'} /></a>
                            <a href={'#'}><img src={GooglePlusLogo} alt={'GooglePlus'} /></a>
                            <a href={'#'}><img src={PinterestLogo} alt={'Pinterest'} /></a>
                        </div>
                    </div>
                    <div className={'our-team__element'}>
                        <div className={'our-team__photo'}>
                            <img src={BlankImage} alt={'Member photo'} />
                        </div>
                        <div className={'our-team__name'}>Alberta Dennis</div>
                        <div className={'our-team__member-desc'}>
                            Fusce nunc risus, fermentum pellentesque dolor at, cursus blandit justo.
                            Nunc sodales nec ante eget.
                        </div>
                        <div className={'our-team__member-contacts'}>
                            <a href={'#'}><img src={TwitterLogo} alt={'Twitter'} /></a>
                            <a href={'#'}><img src={FacebookLogo} alt={'Facebook'} /></a>
                            <a href={'#'}><img src={GooglePlusLogo} alt={'GooglePlus'} /></a>
                            <a href={'#'}><img src={PinterestLogo} alt={'Pinterest'} /></a>
                        </div>
                    </div>
                    <div className={'our-team__element'}>
                        <div className={'our-team__photo'}>
                            <img src={BlankImage} alt={'Member photo'} />
                        </div>
                        <div className={'our-team__name'}>William Park</div>
                        <div className={'our-team__member-desc'}>
                            Nulla in tincidunt neque, a luctus mi. Donec sollicitudin est vehicula
                            mauris condimentum mattis.
                        </div>
                        <div className={'our-team__member-contacts'}>
                            <a href={'#'}><img src={TwitterLogo} alt={'Twitter'} /></a>
                            <a href={'#'}><img src={FacebookLogo} alt={'Facebook'} /></a>
                            <a href={'#'}><img src={GooglePlusLogo} alt={'GooglePlus'} /></a>
                            <a href={'#'}><img src={PinterestLogo} alt={'Pinterest'} /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
